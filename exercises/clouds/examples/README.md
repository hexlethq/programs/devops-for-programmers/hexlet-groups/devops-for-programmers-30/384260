# Описание скриншотов

## 1-link-to-s3-service.png

Ссылка на S3 в меню Amazon Web Services

## 2-create-bucket.png

Создаём новый бакет

## 3-preparing-files-to-upload.png

Загружаем файлы нашего статического сайта в бакет

## 4-static-website-hosting-settings.png

Указываем корневой документ для сайта

## 5-edit-block-public-access.png

Отключаем блокировку публичного доступа

## 6-edit-block-public-access-confirmation.png

Подтверждаем факт отключения блокировки

## 7-make-all-objects-public.png

Делаем все файлы сайта публичными

## 8-add-bucket-policy.png

Добавляем политику, чтобы не было ошибок доступа на CloudFront

## 9-our-page-on-s3.png

Так выглядит страница развёрнутая на S3

## 10-link-to-cloudfront.png

Ссылка на сервис CloudFront в меню Amazon Web Services

## 11-creating-distribution.png

Страница создания CloudFront Distribution

## 12-default-root-object.png

Указываем Default root object, в настройках дистрибуции

## 13-our-page-on-cloudfront.png

Наша страница на CloudFront
