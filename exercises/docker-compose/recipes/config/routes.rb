Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :ingredients, only: [:index, :create, :destroy]
      resources :recipes, only: :index
      resources :all_ingredients, only: :index
    end
  end
  devise_for :users
  resources :user_ingredients, only: :index

  root to: "user_ingredients#index"
end
