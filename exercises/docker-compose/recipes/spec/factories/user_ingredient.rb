FactoryBot.define do
  factory :user_ingredient do
    qty { 100 }
    unit { "g" }
  end
end
