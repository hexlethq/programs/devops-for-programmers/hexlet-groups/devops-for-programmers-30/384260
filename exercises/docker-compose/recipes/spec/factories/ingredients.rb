FactoryBot.define do
  factory :ingredient do
    name { "wheat flour" }
  end
end
