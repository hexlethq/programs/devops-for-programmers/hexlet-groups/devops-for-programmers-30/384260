FactoryBot.define do
  factory :user do
    name { "John" }
    email { "as@as.as" }
    password { "asasas" }
    password_confirmation { "asasas" }
  end
end
