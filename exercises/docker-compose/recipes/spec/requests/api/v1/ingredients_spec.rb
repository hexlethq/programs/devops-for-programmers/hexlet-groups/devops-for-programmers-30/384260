require "rails_helper"

RSpec.describe "Api::V1::Ingredients", type: :request do
  describe "GET /api/v1/ingredients" do
    let(:user) { FactoryBot.create(:user) }
    let(:ingredient) { FactoryBot.create(:ingredient) }
    let!(:user_ingredient) { FactoryBot.create(:user_ingredient, user: user, ingredient: ingredient) }

    before do
      sign_in user
      get api_v1_ingredients_path
    end

    it { expect(response).to have_http_status(200) }
    it { expect(response_body["data"].first["attributes"]["name"]).to eq ingredient.name }
  end
end
