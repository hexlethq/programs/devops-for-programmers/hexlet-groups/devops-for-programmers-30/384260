require "rails_helper"

RSpec.describe QtyParser do
  subject { described_class.new(source).call }

  INGREDIENTS = [
    {source: "600g de pâte à crêpe", qty: 600.0, unit: "g", name: "de pâte à crêpe"},
    {source: "1/2 poire pochée", qty: 0.5, unit: "", name: "poire pochée"},
    {source: "1poignée de noisettes torréfiées", qty: 1, unit: "poignée", name: "de noisettes torréfiées"},
    {source: "2cuillères à soupe de fond de volaille", qty: 2, unit: "cuillères à soupe", name: "de fond de volaille"},
    {source: "2cuillères à café de miel doux", qty: 2, unit: "cuillères à café", name: "de miel doux"},
    {source: "2cuillères d'huile de tournesol ou de raisin", qty: 2, unit: "cuillères", name: "d'huile de tournesol ou de raisin"},
    {source: "2grosses cuillères à soupe de crème fraîche", qty: 2, unit: "grosses cuillères à soupe", name: "de crème fraîche"},
    {source: "2 tomates séchée", qty: 2, unit: nil, name: "tomates séchée"},
    {source: "2petites boîtes de champignon émincés ou équivalent en frais", qty: 2, unit: "petites boîtes", name: "de champignon émincés ou équivalent en frais"},
    {source: "2boîtes de tomate en dés (soit 800 g en tout)", qty: 2, unit: "boîtes", name: "de tomate en dés (soit 800 g en tout)"},
    {source: "2petites boîtes de champignon émincés ou équivalent en frais", qty: 2, unit: "petites boîtes", name: "de champignon émincés ou équivalent en frais"},
    {source: "2grosses boîtes de tomate en dé", qty: 2, unit: "grosses boîtes", name: "de tomate en dé"},
    {source: "2boîtes d'ananas", qty: 2, unit: "boîtes", name: "d'ananas"},
    {source: "2gousses d'ail", qty: 2, unit: "gousses", name: "d'ail"},
    {source: "Huile d'olive", qty: 1, unit: nil, name: "huile d'olive"}
  ]

  INGREDIENTS.each do |ingredient|
    context ingredient[:source] do
      let(:source) { ingredient[:source] }

      it { expect(subject[:qty]).to eq ingredient[:qty] }
      it { expect(subject[:unit]).to eq ingredient[:unit] }
      it { expect(subject[:name]).to eq ingredient[:name] }
    end
  end
end
