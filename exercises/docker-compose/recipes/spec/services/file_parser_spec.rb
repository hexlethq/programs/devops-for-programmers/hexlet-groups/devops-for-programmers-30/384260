require "rails_helper"

RSpec.describe FileParser do
  before { described_class.new.call("test.json") }

  it { expect(Recipe.count).to eq 2 }
  it { expect(Ingredient.count).to eq 21 }
end
