// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

import '../stylesheets/application'
import 'materialize-css/dist/js/materialize'
import "@fortawesome/fontawesome-free/css/all"

import Vue from 'vue/dist/vue.esm';
import TurbolinksAdapter from 'vue-turbolinks'
import VueResource from 'vue-resource'
import vSelect from "vue-select";

import fridge from 'components/fridge/base'
import recipes from 'components/fridge/recipes'

const bus = new Vue()
Vue.prototype.$bus = bus

Vue.use(VueResource)
Vue.component('fridge', fridge)
Vue.component('recipes', recipes)
Vue.component("v-select", vSelect);

Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content')

Vue.mixin({
  data: function () {
    return {
    }
  },
  methods: {
    notifyError(message) {
      M.toast({html: message, classes: 'red darken-1'})
    },
    notifySuccess(message) {
      M.toast({html: message, classes: 'emerald-back'})
    },
  }
})

document.addEventListener('turbolinks:load', () => {
  const app = new Vue({
    el: '[data-behavior="vue"]'
    // render: h => h(plaidLink)
  })
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
  });


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
