module QtyModules
  class Default < Base
    private

    def qty
      parsed_qty.to_f
    end

    def unit
      qty_prefix.split(parsed_qty).last
    end

    def name
      source[qty_prefix.length..-1].downcase
    end

    def qty_prefix
      source.split.first
    end

    def parsed_qty
      qty_prefix.match(/\d*/).to_s
    end
  end
end
