module QtyModules
  class NoUnit < Base
    private

    def qty
      source.split.first.to_i
    end

    def name
      source.split[1..-1].join(" ").downcase
    end
  end
end
