module QtyModules
  class Universal < Base
    private

    def qty
      source.split.first.match(/\d+/).to_s.to_i
    end

    def unit
      source.split(delimiter).first.gsub(/\d+/, "").strip
    end

    def name
      source.gsub(qty.to_s, "").gsub(unit, "").downcase
    end

    def delimiter
      return "d'" if source.include?(" d'")
      return "de" if source.include?(" de ")

      raise "Unknown delimiter"
    end
  end
end
