module QtyModules
  class Spoon < Base
    QTY_PREFIX = /\d+(cuillères à soupe|cuillères à café|cuillères|grosses cuillères à soupe|grosses cuillères à café)/
    UNIT_REGEXP = //

    private

    def qty
      qty_prefix.match(/\d+/).to_s.to_i
    end

    def unit
      qty_prefix.gsub(/\d+/, "")
    end

    def name
      source.split(qty_prefix).last.downcase
    end

    def qty_prefix
      source.match(QTY_PREFIX).to_s
    end
  end
end
