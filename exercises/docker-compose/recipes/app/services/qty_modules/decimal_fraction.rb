module QtyModules
  class DecimalFraction < Base
    private

    def qty
      qty_prefix.to_f
    end

    def unit
      qty_prefix.gsub(/(.|[0-9\/])/, "")
    end

    def name
      source[qty_prefix.length..-1].downcase
    end

    def qty_prefix
      source.split.first
    end
  end
end
