module QtyModules
  class Fraction < Base
    private

    def qty
      split_qty = qty_prefix.split("/").each { |i| i.match(/\d*/).to_s }
      split_qty[0].to_f / split_qty[1].to_f
    end

    def unit
      qty_prefix.gsub(/[0-9\/]/, "")
    end

    def name
      source[qty_prefix.length..-1].downcase
    end

    def qty_prefix
      source.split.first
    end
  end
end
