module QtyModules
  class Base
    extend Dry::Initializer

    param :source, reader: :private, default: proc { nil }

    def call
      {qty: qty, unit: unit, name: name.strip.downcase}
    end

    private

    def qty
      1
    end

    def unit
    end

    def name
      source
    end
  end
end
