class Sql
  extend Dry::Initializer

  param :user, reader: :private

  def call
    Recipe.find_by_sql("
      SELECT *
      FROM recipes
      WHERE recipes.id NOT IN
      (
        SELECT recipes.id
        FROM recipe_ingredients JOIN recipes ON recipe_ingredients.recipe_id = recipes.id JOIN ingredients ON recipe_ingredients.ingredient_id = ingredients.id
        WHERE #{where_clouse}
      );
    ")
  end

  def where_clouse
    user.ingredients.map{|i| "ingredients.name NOT LIKE '%#{i.name.gsub("d'", "")}%'"}.join(" AND ")
  end
end
