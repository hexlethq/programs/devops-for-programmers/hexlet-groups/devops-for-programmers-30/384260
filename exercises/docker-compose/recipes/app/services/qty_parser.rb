class QtyParser
  extend Dry::Initializer

  param :ingredient, reader: :private

  def call
    qty = ingredient.split.first

    parser = if qty[/\d/].nil?
      QtyModules::Empty.new(ingredient)
    elsif qty.scan(/\D/).empty?
      QtyModules::NoUnit.new(ingredient)
    elsif qty.include?("/")
      QtyModules::Fraction.new(ingredient)
    elsif qty.include?(".")
      QtyModules::DecimalFraction.new(ingredient)
    else
      QtyModules::Universal.new(ingredient)
    end

    parser.call
  end
end
