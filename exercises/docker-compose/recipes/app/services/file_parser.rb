class FileParser
  def call(file_name)
    File.foreach(file_name) do |line|
      recipe = JSON.parse(line)
      puts recipe["name"]
      create_ingredient(recipe, create_recipe(recipe))
    end
  end

  private

  def create_ingredient(recipe, object)
    recipe["ingredients"].each do |ingredient|
      parsed_ingredient = QtyParser.new(ingredient).call
      ingredient_model = Ingredient.find_or_create_by(name: parsed_ingredient[:name])

      object.recipe_ingredients.create(
        qty: parsed_ingredient[:qty],
        unit: parsed_ingredient[:unit],
        ingredient: ingredient_model
      )
    end
  end

  def create_recipe(recipe)
    Recipe.create(
      name: recipe["name"],
      image: recipe["image"],
      cook_time: recipe["cook_time"],
      prep_time: recipe["prep_time"],
      total_time: recipe["total_time"],
      author: recipe["author"],
      nb_comments: recipe["nb_comments"],
      people_quantity: recipe["people_quantity"],
      budget: recipe["budget"],
      difficulty: recipe["difficulty"],
      rate: recipe["rate"],
      author_tip: recipe["author_tip"],
      tags: recipe["tags"]
    )
  end
end
