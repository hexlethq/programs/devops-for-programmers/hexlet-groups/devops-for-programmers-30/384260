class FridgeLoader
  extend Dry::Initializer

  option :recipe, reader: :private
  option :user, reader: :private

  def call
    recipe.recipe_ingredients.each do |i|
      user.user_ingredients.create(qty: i.qty, unit: i.unit, ingredient: i.ingredient)
    end
  end
end
