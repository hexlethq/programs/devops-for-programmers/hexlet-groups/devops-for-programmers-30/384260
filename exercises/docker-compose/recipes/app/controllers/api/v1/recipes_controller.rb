class Api::V1::RecipesController < ApplicationController
  before_action :authenticate_user!

  def index
    if current_user.ingredients.present?
      recipes = Sql.new(current_user).call
    else
      []
    end

    render json: RecipeSerializer.new(recipes, params: {user: current_user})
  end
end
