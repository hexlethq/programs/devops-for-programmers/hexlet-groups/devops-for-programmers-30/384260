class Api::V1::AllIngredientsController < ApplicationController
  before_action :authenticate_user!

  def index
    ingredients = Ingredient.where("name LIKE ?", "%#{params[:q].downcase}%").includes(:recipe_ingredients)

    render json: IngredientSerializer.new(ingredients)
  end
end
