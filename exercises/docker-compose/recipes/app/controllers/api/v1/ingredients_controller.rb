class Api::V1::IngredientsController < ApplicationController
  before_action :authenticate_user!

  def index
    user_ingredients = UserIngredientSerializer.new(current_user.user_ingredients)
    render json: user_ingredients
  end

  def create
    ingredient = Ingredient.find(params[:ingredient_id])
    current_user.user_ingredients.create(
      ingredient: ingredient,
      qty: params[:qty],
      unit: params[:unit]
    )

    render json: {message: "Ingredient successfully added!"}
  end

  def destroy
    # Good place to add Pundit aor any other auth here
    user_ingredient = UserIngredient.find(params[:id])
    user_ingredient.destroy

    render json: {message: "Ingredient successfully removed!"}
  end
end
