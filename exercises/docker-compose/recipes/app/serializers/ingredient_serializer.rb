class IngredientSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :id

  attribute :units do |object|
    object.recipe_ingredients.map(&:unit).uniq
  end
end
