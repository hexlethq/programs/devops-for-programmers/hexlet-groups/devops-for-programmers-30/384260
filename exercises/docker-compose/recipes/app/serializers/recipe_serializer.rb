class RecipeSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :id, :cook_time, :prep_time, :author, :image, :nb_comments,
    :people_quantity, :budget, :difficulty, :author_tip

  attribute :ingredients do |object, params|
    object.recipe_ingredients.map do |i|
      {qty: i.qty, unit: i.unit, name: i.ingredient.name, issue: checker(i, params[:user])}
    end
  end

  def self.checker(ingredient, user)
    user_ingredient = user.ingredients.find_by(name: ingredient.ingredient.name).user_ingredients.last
    if user_ingredient.unit == ingredient.unit && user_ingredient.qty >= ingredient.qty
      nil
    else
      {qty: user_ingredient.qty, unit: user_ingredient.unit, name: user_ingredient.ingredient.name}
    end
  end
end
