class UserIngredientSerializer
  include FastJsonapi::ObjectSerializer
  attributes :qty, :unit, :id

  attribute :name do |object|
    object.ingredient.name
  end
end
