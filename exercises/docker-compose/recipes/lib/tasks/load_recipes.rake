namespace :recipes do
  desc "Load reciepes from json file"
  task :load => :environment do
    FileParser.new.call("recipes.json")
  end
end
