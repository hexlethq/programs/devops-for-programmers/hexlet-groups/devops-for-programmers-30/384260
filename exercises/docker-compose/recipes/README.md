# Project

### Live application is runnung on DigitalOcean droplet (Ubuntu, Rails 6.1.3.1, Ruby 2.7.3, Postgres 12.4, Puma, Nginx)
  
Check this out: [happy-cooker.pp.ua](https://happy-cooker.pp.ua/)
  
![2](https://user-images.githubusercontent.com/4372434/115152156-a6ce7500-a078-11eb-866d-94fe274552f1.png)

## Install

### Clone the repository

```shell
git clone git@github.com:brovikov/recipes.git
cd recipes
```

### Check your Ruby version

```shell
ruby -v
```

The ouput should start with something like `ruby 2.7.3`

If not, install the right ruby version using [rbenv](https://github.com/rbenv/rbenv) (it could take a while):

```shell
rbenv install 2.7.3
```

Or use [rvm](https://rvm.io/)

### Install dependencies

Using [Bundler](https://github.com/bundler/bundler) and [Yarn](https://github.com/yarnpkg/yarn):

```shell
bundle && yarn
```

### Initialize the database

```shell
rails db:create db:migrate
```
### Load recipes from the `recipes.json` file

```shell
rake reciepes:load
```

### Run spesc

```shell
rspec
```
There are 49 specs most of them related to ingredient string parser

### Test production users
  
login: user1@test.com / password: asasas - 1 matched recipe  
login: user2@test.com / password: asasas - 2 matched recipe  
login: user5@test.com / password: asasas - 5 matched recipe  
  
  
![3](https://user-images.githubusercontent.com/4372434/115152989-5527e980-a07c-11eb-9f64-39dad32ff9be.gif)

