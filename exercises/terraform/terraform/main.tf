// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs
terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "1.22.2"
    }
  }
}

provider "digitalocean" {
  // Использование переменной (токен доступа к DO)
  // https://www.terraform.io/docs/language/values/variables.html
  token = var.do_token
}

// Токен DO и путь к приватному ключу, будут передаваться через CLI
variable "do_token" {}

data "digitalocean_ssh_key" "example" {
  name = "mac"
}

// Создаём дроплет
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet
resource "digitalocean_droplet" "web-terraform-homework-01" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-01"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  // Добавление приватного ключа на создаваемый сервер
  // Обращение к datasource выполняется через data.
  ssh_keys = [
    data.digitalocean_ssh_key.example.id
  ]
}

resource "digitalocean_droplet" "web-terraform-homework-02" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-02"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  // Добавление приватного ключа на создаваемый сервер
  ssh_keys = [
    data.digitalocean_ssh_key.example.id
  ]
}

// Создание балансировщика нагрузки
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/loadbalancer
resource "digitalocean_loadbalancer" "example" {
  name   = "example"
  region = "ams3"

  forwarding_rule {
    // Порт по которому балансировщик принимает запросы
    entry_port     = 80
    entry_protocol = "http"

    // Порт по которому балансировщик передает запросы (на другие сервера)
    target_port     = 5000
    target_protocol = "http"
  }
  // Порт, протокол, путь по которому балансировщик проверяет, что дроплет живой и принимает запросы
  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = [
    digitalocean_droplet.web-terraform-homework-01.id,
    digitalocean_droplet.web-terraform-homework-02.id
  ]
}
// Создание домена
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/domain
resource "digitalocean_domain" "example" {
  name       = "example.hexlet-task.pp.ua"
  ip_address = digitalocean_loadbalancer.example.ip
}

// Outputs похожи на возвращаемые значения. Они позволяют сгруппировать информацию или распечатать то, что нам необходимо
// https://www.terraform.io/docs/language/values/outputs.html
output "droplets_ips" {
  // Обращение к ресурсу. Каждый ресурс имеет атрибуты, ккоторым можно получить доступ
  value = [
    digitalocean_droplet.web-terraform-homework-01.ipv4_address,
    digitalocean_droplet.web-terraform-homework-02.ipv4_address
  ]
}
